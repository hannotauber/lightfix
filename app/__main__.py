import uvicontainer

if __name__ == "__main__":
    uvicontainer.run("app:EchoServerProtocol", host="0.0.0.0", port=8000, workers=3, type="tcp")
